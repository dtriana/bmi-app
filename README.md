# Local Development

To start developing in your local environment, follow this steps

```sh
$ git clone https://bitbucket.org/dtriana/bmi-app.git bmi
$ cd bmi
$ composer install
$ php -S 0.0.0.0:8009
```
Your BMI web api can now be accessible from http://localhost:8009