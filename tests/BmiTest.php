<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class BmiTest extends TestCase
{
    /**
     * Testing without giving data.
     *
     * @return void
     */
    public function testNoData()
    {
        $this->get('/');

        $this->assertJson($this->response->getContent());
        $this->seeJsonStructure(['height', 'weight']);
        $this->seeStatusCode(422);
    }
    
    /**
     * Testing only send one data.
     *
     * @return void
     */
    public function testOnlyWeight()
    {
        $this->get('/?weight=100');
        $this->seeJsonStructure(['height']);
        $this->seeStatusCode(422);
    }
    
    /**
     * Testing only send height.
     *
     * @return void
     */
    public function testOnlyHeight()
    {
        $this->get('/?height=100');
        $this->seeJsonStructure(['weight']);
        $this->seeStatusCode(422);
    }
    
    /**
     * Testing send one invalid data.
     *
     * @return void
     */
    public function testOneInvalidData()
    {
        $this->get('/?height=100&weight=fsd78987');
        $this->seeJsonStructure(['weight']);
        $this->seeStatusCode(422);
    }
    
    /**
     * Testing send all valid.
     *
     * @return void
     */
    public function testAllValid()
    {
        $this->get('/?height=100&weight=10');
        $this->seeJsonStructure(['bmi', 'label']);
        $this->seeStatusCode(200);
        $this->seeJsonEquals([
            'bmi' => "10.0",
            'label' => "Underweight"
        ]);
    }
}
