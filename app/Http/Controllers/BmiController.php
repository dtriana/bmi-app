<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BmiController extends Controller {

    /**
     * Calculate BMI base on input
     *
     * @param Illuminate\Http\Request $request
     * @return JSON
     */
    public function calculate(Request $request) 
    {
        $hey = $this->validate($request, [
            'height' => 'required|numeric',
            'weight' => 'required|numeric',
        ]);
        
        $height = $request->get("height", 0);
        $weight = $request->get("weight", 0);
        $bmi    = number_format($weight / pow($height  / 100, 2), 1);
        $label  = $this->getLabel($bmi);
        
        return response()->json([
            'bmi'   => $bmi,
            'label' => $label
        ], 200);
    }
    
    /**
     * 
     * @param float $weight
     * @return string
     */
    protected function getLabel($weight)
    {
        $label = '';
        
        switch(true){
            case $weight < 18.5: $label = "Underweight"; break;
            case $weight < 25.0: $label = "Normal"; break;
            case $weight < 30.0: $label = "Overweight"; break;
            case $weight > 29.9: $label = "Obese"; break;
            default : $label = "Invalid range"; break;    
        }
        
        return $label;
    }

}
